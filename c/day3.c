#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

struct Point {
	int x;
	int y;
};

struct Segment {
	struct Point start;
	struct Point end;
};

struct Wire {
	struct Segment data;
	struct Wire* next;
};

static const char* const delim = ",";

bool intersects(const struct Segment, const struct Segment);

int main()
{
	FILE* input = fopen("../day3.txt", "r");

	// get size req for the wires
	int size = 0;
	int temp = 0;
	while (fgetc(input) != '\n')
		temp++;
	while (fgetc(input) != EOF)
		size++;
	if (temp > size)
		size = temp;
	rewind(input);

	// read first wire
	char* in = malloc(size + 1);
	fscanf(input, "%s", in);

	// gets first token
	char* instr = strtok(in, delim);

	struct Point current = {0, 0};
	// create first wire
	struct Wire* wire = malloc(sizeof(struct Wire));
	wire->data.start = current;
	wire->data.end = current;
	wire->next = NULL;

	char dir;
	int amt;
	// loop through each token
	while (instr != NULL) {
		// process token
		sscanf(instr, "%c%d", &dir, &amt);
		struct Segment temp;
		temp.start = current;
		
		switch (dir) {
			case 'R':
				temp.end.x = temp.start.x + amt;
				temp.end.y = temp.start.y;
				break;
			case 'L':
				temp.end.x = temp.start.x - amt;
				temp.end.y = temp.start.y;
				break;
			case 'U':
				temp.end.x = temp.start.x;
				temp.end.y = temp.start.y + amt;
				break;
			case 'D':
				temp.end.x = temp.start.x;
				temp.end.y = temp.start.y - amt;
				break;
		}
		// insert temp into wire linked list
		struct Wire* new = malloc(sizeof(struct Wire));
		new->data = temp;
		new->next = wire;
		wire = new;

		instr = strtok(NULL, delim);
	}
	// wire 1 is complete

	// parse wire 2
	fscanf(input, "%s", in);
	instr = strtok(in, delim);
	current.x = current.y = 0;
	while (instr != NULL) {
		// process token
		sscanf(instr, "%c%d", &dir, &amt);
		struct Segment temp;
		temp.start = current;
		
		switch (dir) {
			case 'R':
				temp.end.x = temp.start.x + amt;
				temp.end.y = temp.start.y;
				break;
			case 'L':
				temp.end.x = temp.start.x - amt;
				temp.end.y = temp.start.y;
				break;
			case 'U':
				temp.end.x = temp.start.x;
				temp.end.y = temp.start.y + amt;
				break;
			case 'D':
				temp.end.x = temp.start.x;
				temp.end.y = temp.start.y - amt;
				break;
		}
		// TODO: call intersects and compare
		struct Wire* search = wire;
		while (search != NULL) {
			if (intersects(search->data, temp)) {
				
			}
		}
		instr = strtok(NULL, delim);
	}

	// free linked list
	struct Wire* tmp = wire;
	while (wire != NULL) {
		tmp = wire;
		wire = wire->next;
		free(tmp);
	}

	free(in);
	fclose(input);
}

bool
intersects(const struct Segment a, const struct Segment b)
{
	return false;
}

