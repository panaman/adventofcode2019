#include <stdio.h>

int main()
{
	FILE* input = fopen("../day1.txt", "r");

	int sum1 = 0;
	int sum2 = 0;
	int temp;
	while (fscanf(input, "%d", &temp) != EOF) {
		temp = (temp / 3) - 2;
		sum1 += temp;
		while (temp > 0) {
			sum2 += temp;
			temp = (temp / 3) - 2;
		}
	}
	printf("%d\n", sum1);
	printf("%d\n", sum2);

	fclose(input);
}

