#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "intcode.h"

int main()
{
	FILE* input = fopen("../day2.txt", "r");

	int temp;
	int size = 0;
	while (fscanf(input, "%d,", &temp) != EOF)
		size++;
	rewind(input);

	int* prog = malloc(size * sizeof(int));
	read_file(input, prog);
	fclose(input);

	int* copy = malloc(size * sizeof(int));
	memcpy(copy, prog, size * sizeof(int));

	copy[1] = 12;
	copy[2] = 2;
	exec(copy);
	printf("%d\n", copy[0]);

	free(prog);
	free(copy);
}

