#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define NUM_PAIRS 1233

struct Planet {
	struct Planet* child;
	struct Planet* next;
	char name[4];
};

struct Pair {
	char center[4];
	char orbit[4];
};

int main()
{
	FILE* input = fopen("../day6.txt", "r");

	struct Pair pairs[NUM_PAIRS];

	int pos = 0;
	struct Pair temp;
	while (fscanf(input, "%3s)%3s", temp.center, temp.orbit) != EOF)
		pairs[pos++] = temp;
	fclose(input);

	printf("%s -> %s\n", pairs[3].center, pairs[3].orbit);
}

