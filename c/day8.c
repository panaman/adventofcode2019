#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

#define INPUT_SIZE 15000
#define WIDTH 25
#define HEIGHT 6
#define LAYER_SIZE 150
#define NUM_LAYERS 100

void part1(const char* const);
void part2(const char* const);

int main()
{
	FILE* input = fopen("../day8.txt", "r");
	char* const image = malloc(INPUT_SIZE);

	for (int i = 0; i < INPUT_SIZE; i++)
		fscanf(input, "%c", &image[i]);
	fclose(input);

	part1(image);
	part2(image);

	free(image);
}

void
part1(const char* const image)
{
	int ans[3] = {INT_MAX};
	int zeros, ones, twos;
	for (int k = 0; k < NUM_LAYERS; k++) {
		zeros = ones = twos = 0;
		for (int q = k * LAYER_SIZE; q < (k + 1) * LAYER_SIZE; q++) {
			switch (image[q]) {
				case '0': zeros++; break;
				case '1': ones++;  break;
				case '2': twos++;  break;
			}
		}
		if (zeros < ans[0]) {
			ans[0] = zeros;
			ans[1] = ones;
			ans[2] = twos;
		}
	}
	printf("%d\n", ans[1] * ans[2]);
}

void
part2(const char* const image)
{
	char render[HEIGHT][WIDTH];

	for (int i = NUM_LAYERS - 1; i >= 0; i--) {
		for (int q = i * LAYER_SIZE; q < (i + 1) * LAYER_SIZE; q++) {
			if (image[q] == '2')
				continue;

			int idx = q % LAYER_SIZE;
			render[idx / WIDTH][idx % WIDTH] = image[q] == '0' ? ' ' : '#';
		}
	}
	// display image
	for (int y = 0; y < HEIGHT; y++) {
		for (int x = 0; x < WIDTH; x++) {
			printf("%c ", render[y][x]);
		}
		putchar('\n');
	}
}

