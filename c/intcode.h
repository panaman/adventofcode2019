#ifndef INTCODE_H
#define INTCODE_H

void exec(int* const);
void read_file(FILE*, int* const);

#endif

