#include <stdio.h>
#include "intcode.h"

void
exec(int* const copy)
{
	// program start
	int i = 0;
	while (copy[i] != 99) {
		int num = copy[i];
		const char opcode = num % 100;
		num /= 100;
		const char mode1 = num % 10;
		num /= 10;
		const char mode2 = num % 10;
		num /= 10;
		const char mode3 = num % 10;

		int args[3];
		// get args
		switch (opcode) {
			case 1: // add
			case 2: // mul
				switch (mode1) {
					case 0: // pos
						args[0] = copy[copy[i + 1]];
						break;
					case 1: // imm
						args[0] = copy[i + 1];
						break;
				}
				switch (mode2) {
					case 0:
						args[1] = copy[copy[i + 2]];
						break;
					case 1:
						args[1] = copy[i + 2];
						break;
				}
				switch (mode3) {
					case 0:
						args[2] = copy[copy[i + 3]];
						break;
					case 1:
						args[2] = copy[i + 3];
						break;
				}
				break;
			case 3:
			case 4:
				break;
			case 99:
				break;
		}

		// execute instr
		switch (opcode) {
			case 0:
				copy[args[2]] = copy[args[0]] + copy[args[1]];
				i += 4;
				break;
			case 1:
				copy[args[2]] = copy[args[0]] * copy[args[1]];
				i += 4;
				break;
			case 3:
			case 4:
				i += 2;
				break;
		}
	}
}

void
read_file(FILE* input, int* const prog)
{
	int i = 0;
	while (fscanf(input, "%d,", &prog[i++]) != EOF);
}

