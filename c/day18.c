#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

struct Node {
	int x;
	int y;
	char label;
};

int main()
{
	FILE* input = fopen("../day18.txt", "r");

	int width = 0;
	while (fgetc(input) != '\n')
		width++;
	rewind(input);
	fseek(input, 0L, SEEK_END);
	int numRows = (ftell(input) - 2) / width;
	rewind(input);

	struct Node doors[26];
	struct Node keys[26];
	char grid[numRows][width];

	int dPos = 0;
	int kPos = 0;
	for (int y = 0; y < numRows; y++) {
		for (int x = 0; x < width; x++) {
			char temp;
		check:
			temp = fgetc(input);
			switch (temp) {
				case '#':
				case '.':
					grid[y][x] = temp;
					break;
			}
		}
	}

	fclose(input);
}

