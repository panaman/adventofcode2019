#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#define WIDTH 10
//#define WIDTH 36
#define VAPOR 200

struct Point {
	char x;
	char y;
};

struct Map {
	float angle;
	struct Point points[];
};

struct Point part1(struct Point*, int);
void part2(struct Point, struct Point*, int);
int cmp(const void*, const void*);

int main()
{
	FILE* input = fopen("../test.txt", "r");

	// Get number of asteroids
	char c;
	int len = 0;
	while ((c = fgetc(input)) != EOF) {
		if (c == '#')
			len++;
	}
	rewind(input);

	struct Point asts[len];

	// Fill asts array with positions of asteriods
	int pos = 0;
	char line[WIDTH + 1];
	for (char y = 0; y < WIDTH; y++) {
		fscanf(input, "%10s%*c", line);
		for (char x = 0; x < WIDTH; x++) {
			if (line[x] == '#') {
				struct Point temp = { x, y };
				asts[pos++] = temp;
			}
		}
	}
	fclose(input);

	// Get position of base
	struct Point base = part1(asts, len);

	// Remove base from set of asteroids
	for (int i = 0; i < len; i++) {
		// If current asteroid is the base
		if (asts[i].x == base.x && asts[i].y == base.y) {
			// Backswap and adjust len
			asts[i] = asts[(len--) - 1];
			break;
		}
	}
	//printf("Base: %d, %d\n", base.x, base.y);
	part2(base, asts, len);
}

struct Point
part1(struct Point* asts, int len)
{
	int bestAmt = 0;
	struct Point station;
	float* seen = malloc(len * sizeof(float));
	for (int cur = 0; cur < len; cur++) {
		int detectAmt = 0;
		for (int tgt = 0; tgt < len; tgt++) {
			if (cur != tgt) {
				float angle = atan2(asts[tgt].y - asts[cur].y,
						asts[tgt].x - asts[cur].x);
				for (int q = 0; q < detectAmt; q++) {
					if (seen[q] == angle)
						goto exists;
				}
				seen[detectAmt++] = angle;
			}
		exists: ;
		}
		if (detectAmt > bestAmt) {
			bestAmt = detectAmt;
			station = asts[cur];
		}
	}
	printf("Part1: %d\n", bestAmt);
	free(seen);

	return station;
}

void
part2(struct Point base, struct Point* asts, int len)
{
/*
	1. hashmap<angle, Vec<Point>>
	2. sort buckets by distance from base
	3. loop hashmap starting at 12 oclock
	4. pop out closest element to base and move to next bucket
	5. do 200 times and see which asteroid you are on
*/
	const float PI = acosf(-1);
	const float val = 180 / PI;

	float* angles = malloc(len * sizeof(float));
	struct Map* map = calloc(len, sizeof(struct Map));

	for (int i = 0; i < len; i++) {
		int dy = (int)asts[i].y - base.y;
		int dx = (int)asts[i].x - base.x;

		// Convert to degrees
		angles[i] = atan2f((float)dy, (float)dx) * val;

		// Remap (-180, 180) to (0, 360)
		float ang = angles[i];
		if (ang < 0)
			angles[i] += 360;
		// rotate left 90 degrees
		angles[i] -= 90;
	}

	qsort(angles, len, sizeof(float), cmp);
/*
	for (int i = 0; i < len; i++)
		printf("%f\n", angles[i]);
*/
	

	free(angles);
	free(map);
}

int
cmp(const void* a, const void* b)
{
	return (*(float*)a - *(float*)b);
}
