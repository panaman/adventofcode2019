#include <stdio.h>
#include <stdbool.h>

#define INPUT_LOWER 145852
#define INPUT_UPPER 616942

int main()
{
	int amtMin = 0;
	int amtExact = 0;
	for (int i = INPUT_LOWER; i <= INPUT_UPPER; i++) {
		int num = i;
		bool min2 = false;
		bool exact2 = false;
		while (num) {
			int group = 0;
			int prev = num % 10;
			int curr;
			do {
				group++;
				num /= 10;
				curr = num % 10;
			} while (curr == prev);
			if (curr > prev)
				goto next;
			if (group == 2) {
				min2 = true;
				exact2 = true;
			}
			else if (group > 2)
				min2 = true;
		}
		if (min2)
			amtMin++;
		if (exact2)
			amtExact++;
	next: ;
	}
	printf("part1: %d\npart2: %d\n", amtMin, amtExact);
}

