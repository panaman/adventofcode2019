#include <stdio.h>
#include <stdlib.h>

#define STEPS 1000
//#define STEPS 10

struct Moon {
	struct {
		int x;
		int y;
		int z;
	} pos;
	struct {
		int x;
		int y;
		int z;
	} vel;
};

void calcVelocity(struct Moon, struct Moon);
void applyVelocity();

// Guesses:
int main()
{
	struct Moon moons[4] = {
		{ // Io
			.pos = {3, 3, 0},
			.vel = {0, 0, 0}
		},
		{ // Europa
			.pos = {4, -16, 2},
			.vel = {0, 0, 0}
		},
		{ // Ganymede
			.pos = {-10, -6, 5},
			.vel = {0, 0, 0}
		},
		{ // Callisto
			.pos = {-3, 0, -13},
			.vel = {0, 0, 0}
		}
	};
/*
	struct Moon moons[4] = {
		{ // Io
			.pos = {-1, 0, 2},
			.vel = {0, 0, 0}
		},
		{ // Europa
			.pos = {2, -10, -7},
			.vel = {0, 0, 0}
		},
		{ // Ganymede
			.pos = {4, -8, 8},
			.vel = {0, 0, 0}
		},
		{ // Callisto
			.pos = {3, 5, -1},
			.vel = {0, 0, 0}
		}
	};
*/
	// Simulate for each step
	for (int i = 0; i < STEPS; i++) {
		for (int curr = 0; curr < 3; curr++) {
			for (int next = curr + 1; next < 4; next++) {
				// Gravity adjustment
				int diff = moons[curr].pos.x - moons[next].pos.x;
				if (diff > 0) {
					moons[curr].vel.x--;
					moons[next].vel.x++;
				}
				else if (diff < 0) {
					moons[curr].vel.x++;
					moons[next].vel.x--;
				}

				diff = moons[curr].pos.y - moons[next].pos.y;
				if (diff > 0) {
					moons[curr].vel.y--;
					moons[next].vel.y++;
				}
				else if (diff < 0) {
					moons[curr].vel.y++;
					moons[next].vel.y--;
				}

				diff = moons[curr].pos.z - moons[next].pos.z;
				if (diff > 0) {
					moons[curr].vel.z--;
					moons[next].vel.z++;
				}
				else if (diff < 0) {
					moons[curr].vel.z++;
					moons[next].vel.z--;
				}
			}
		}
		// For every moon
		for (int j = 0; j < 4; j++) {
			// Apply velocity change to each axis
			moons[j].pos.x += moons[j].vel.x;
			moons[j].pos.y += moons[j].vel.y;
			moons[j].pos.z += moons[j].vel.z;
		}
	}

	// Calculate total energy of system
	int total = 0;
	for (int i = 0; i < 4; i++) {
		int pot = abs(moons[i].pos.x)
				+ abs(moons[i].pos.y)
				+ abs(moons[i].pos.z);

		int kin = abs(moons[i].vel.x)
				+ abs(moons[i].vel.y)
				+ abs(moons[i].vel.z);
		total += pot * kin;
	}
	printf("Total energy: %d\n", total);
}
