#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "intcode.h"

#define INPUT_SIZE 129
#define TARGET 19690720

void part1(int*, int*);
void part2(int*, int*);
void run_program(const int* const, int* const, int, int);

int main()
{
	int* const prog = malloc(INPUT_SIZE * sizeof(int));

	int i = 0;
	FILE* input = fopen("../day2.txt", "r");
	while (fscanf(input, "%d,", &prog[i++]) != EOF);
	fclose(input);
	
	int* const copy = malloc(INPUT_SIZE * sizeof(int));
	memcpy(copy, prog, INPUT_SIZE * sizeof(int));

	part1(prog, copy);
	part2(prog, copy);

	free(prog);
	free(copy);
}

void
part1(int* prog, int* copy)
{
	run_program(prog, copy, 12, 2);
	printf("%d\n", copy[0]);
}

void
part2(int* prog, int* copy)
{
	for (int noun = 0; noun < 100; noun++) {
		for (int verb = 0; verb < 100; verb++) {
			run_program(prog, copy, noun, verb);
			if (copy[0] == TARGET) {
				printf("%2d%2d\n", noun, verb);
				goto end;
			}
		}
	}
end: ;
}

void
run_program(const int* const prog, int* const copy, int noun, int verb)
{
	memcpy(copy, prog, INPUT_SIZE * sizeof(int));
	copy[1] = noun;
	copy[2] = verb;

	// program start
	int i = 0;
	while (copy[i] != 99) {
		// adding
		if (copy[i] == 1)
			copy[copy[i + 3]] = copy[copy[i + 1]] + copy[copy[i + 2]];
		// multiplying
		else if (copy[i] == 2)
			copy[copy[i + 3]] = copy[copy[i + 1]] * copy[copy[i + 2]];
		i += 4;
	}
}

